<?php
declare(strict_types=1);

namespace PT;

use InvalidArgumentException;
use JsonException;
use PT\Fee\FeeCalculatingException;
use PT\Fee\FeeCalculatingService;
use PT\Transaction\TransactionDeserializer;
use RuntimeException;

class Processor
{
    public function __construct(
        private readonly FeeCalculatingService $feeCalculatingService,
        private readonly TransactionDeserializer $transactionDeserializer,
    )
    {
    }

    public function process(string $filename): void
    {
        $resource = fopen($filename, "r");
        if (!$resource) {
            throw new RuntimeException('Cant open the file ' . $filename);
        }

        try {
            while (($line = fgets($resource)) !== false) {
                try {
                    $lineDecoded = json_decode($line, true, flags: JSON_THROW_ON_ERROR);
                } catch (JsonException $jsonException) {
                    echo 'Transaction parsing failed. Invalid json format: ' . $jsonException->getMessage() . PHP_EOL;
                    continue;
                }

                try {
                    $transactionDto = $this->transactionDeserializer->fromArray($lineDecoded);
                } catch (InvalidArgumentException $ex) {
                    echo 'Transaction parsing failed. ' . $ex->getMessage() . PHP_EOL;
                    continue;
                }

                try {
                    $fee = $this->feeCalculatingService->forTransaction($transactionDto);
                } catch (FeeCalculatingException $ex) {
                    echo 'Fees calculating failed. ' . $ex->getMessage() . PHP_EOL;
                    continue;
                }

                echo round($fee, 2, PHP_ROUND_HALF_EVEN) . PHP_EOL;
            }
        } finally {
            fclose($resource);
        }
    }
}
