<?php
declare(strict_types=1);

namespace PT\Fee;

use Exception;
use PT\BINProvider\BINProviderInterface;
use PT\Currency\CurrencyEnum;
use PT\CurrencyRatesProvider\CurrencyRatesProviderInterface;
use PT\Transaction\TransactionDto;

class FeeCalculatingService
{
    private const FEE_PERCENT_EU = 0.01;
    private const FEE_PERCENT_NON_EU = 0.02;

    public function __construct(
        private readonly BINProviderInterface $binProvider,
        private readonly CurrencyRatesProviderInterface $currencyRatesProvider,
    ) {}

    /**
     * @throws FeeCalculatingException
     */
    public function forTransaction(TransactionDto $transactionDto): float {
        $rate = $this->currencyRatesProvider->getRate(CurrencyEnum::EUR, $transactionDto->currency);
        if (!$rate) {
            throw new FeeCalculatingException("Can't retrieve rate");
        }

        $amountInEUR = $transactionDto->amount / $rate;

        $binInfo = $this->binProvider->getInfo($transactionDto->bin);
        if (!$binInfo) {
            throw new FeeCalculatingException("Can't retrieve info for BIN: " . $transactionDto->bin);
        }

        $fee = $this->isEU($binInfo->countryCode) ? self::FEE_PERCENT_EU : self::FEE_PERCENT_NON_EU;

        return $amountInEUR * $fee;
    }

    private function isEU(string $country): bool
    {
        switch (strtoupper($country)) {
            case 'AT':
            case 'BE':
            case 'BG':
            case 'CY':
            case 'CZ':
            case 'DE':
            case 'DK':
            case 'EE':
            case 'ES':
            case 'FI':
            case 'FR':
            case 'GR':
            case 'HR':
            case 'HU':
            case 'IE':
            case 'IT':
            case 'LT':
            case 'LU':
            case 'LV':
            case 'MT':
            case 'NL':
            case 'PO':
            case 'PT':
            case 'RO':
            case 'SE':
            case 'SI':
            case 'SK':
                return true;
            default:
                return false;
        }
    }
}
