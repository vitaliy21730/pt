<?php
declare(strict_types=1);

namespace PT\Fee;

use Exception;

class FeeCalculatingException extends Exception
{
}
