<?php
declare(strict_types=1);

namespace PT\Transaction;

use InvalidArgumentException;
use PT\Currency\CurrencyEnum;

class TransactionDeserializer
{
    /**
     * @throws InvalidArgumentException
     */
    public function fromArray(array $data) : TransactionDto {
        if (!isset($data['bin'])) {
            throw new InvalidArgumentException('Bin should be presented');
        }
        $rez = filter_var($data['bin'], FILTER_VALIDATE_INT);
        if ($rez === false || is_bool($data['bin'])) {
            throw new InvalidArgumentException('Bin should be valid integer format');
        }
        $bin = (int)$rez;

        if (!isset($data['amount'])) {
            throw new InvalidArgumentException('Amount should be presented');
        }
        $rez = filter_var($data['amount'], FILTER_VALIDATE_FLOAT);
        if ($rez === false || is_bool($data['amount'])) {
            throw new InvalidArgumentException('Amount should be valid float value');
        }
        $amount = (float)$rez;

        if (!isset($data['currency'])) {
            throw new InvalidArgumentException('Currency should be presented');
        }
        if (!is_string($data['currency'])) {
            throw new InvalidArgumentException('Currency should be valid currency code');
        }
        $currency = CurrencyEnum::fromCode($data['currency']);
        if (!$currency) {
            throw new InvalidArgumentException('Currency should be valid currency code');
        }

        return new TransactionDto($bin, $amount, $currency);
    }
}
