<?php
declare(strict_types=1);

namespace PT\Transaction;

use PT\Currency\CurrencyEnum;

class TransactionDto
{
    public function __construct(
        public readonly int $bin,
        public readonly float $amount,
        public readonly CurrencyEnum $currency,
    ) {}
}
