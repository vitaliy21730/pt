<?php
declare(strict_types=1);

namespace PT\CurrencyRatesProvider;

use PT\Currency\CurrencyEnum;

interface CurrencyRatesProviderInterface
{
    // How much you can get currency $to per 1 unit of currency $from
    public function getRate(CurrencyEnum $from, CurrencyEnum $to): ?float;
}
