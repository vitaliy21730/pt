<?php
declare(strict_types=1);

namespace PT\CurrencyRatesProvider;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonException;
use PT\Currency\CurrencyEnum;
use RuntimeException;

class ExchangeratesapiCurrencyRatesProvider implements CurrencyRatesProviderInterface
{
    private Client $client;

    public function __construct(
        private readonly string $accessKey,
        bool $https,
    )
    {
        $protocol = $https ? 'https' : 'http';

        $this->client = new Client([
            'timeout' => 5,
            'base_uri' => $protocol . '://api.exchangeratesapi.io',
        ]);
    }

    public function getRate(CurrencyEnum $from, CurrencyEnum $to): ?float
    {
        if ($from === $to) {
            return 1;
        }

        try {
            $response = $this->client->get('/latest', [
                RequestOptions::QUERY => [
                    'access_key' => $this->accessKey,
                    'base' => $from->code(),
                ]
            ]);
        } catch (GuzzleException $e) {
            throw new RuntimeException('Request failed', previous: $e);
        }

        if ($response->getStatusCode() !== 200) {
            throw new RuntimeException('Status code is invalid: ' . $response->getStatusCode());
        }

        $rawBody = $response->getBody()->getContents();
        try {
            $body = json_decode($rawBody, true, flags: JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new RuntimeException('Response body decoding error. Body: ' . $rawBody, previous: $e);
        }

        if (empty($rate = $body['rates'][$to->code()])) {
            return null;
        }

        return $rate;
    }
}
