<?php
declare(strict_types=1);

namespace PT\BINProvider;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use RuntimeException;

class LookupBinlistBINProvider implements BINProviderInterface
{
    private Client $client;
    public function __construct()
    {
        $this->client = new Client([
            'timeout' => 5,
            'base_uri' => 'https://lookup.binlist.net/',
        ]);
    }

    public function getInfo(int $bin): ?BINInfo
    {
        try {
            $response = $this->client->get("/$bin");
        } catch (GuzzleException $e) {
            throw new RuntimeException('Request failed for BIN: ' . $bin, previous: $e);
        }

        if ($response->getStatusCode() !== 200) {
            throw new RuntimeException('Status code is invalid: ' . $response->getStatusCode());
        }

        $rawBody = $response->getBody()->getContents();
        try {
            $body = json_decode($rawBody, true, flags: JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new RuntimeException('Response body decoding error. Body: ' . $rawBody, previous: $e);
        }

        if (empty($countryCode = $body['country']['alpha2'])) {
            return null;
        }

        return new BINInfo($countryCode);
    }
}
