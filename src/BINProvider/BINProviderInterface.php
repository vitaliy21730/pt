<?php
declare(strict_types=1);

namespace PT\BINProvider;

interface BINProviderInterface
{
    public function getInfo(int $bin): ?BINInfo;
}
