<?php
declare(strict_types=1);

namespace PT\BINProvider;

class BINInfo
{
    public function __construct(
        public readonly string $countryCode,
    )
    {
    }
}
