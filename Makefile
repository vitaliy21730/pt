# Dev commands
run:
	docker build -t pt-php-base -f ./docker/Dockerfile-base . && \
	docker-compose -f ./docker/docker-compose.yml up -d php --build

it:
	docker exec -it pt_php sh

logs:
	docker logs pt_php

down:
	docker-compose -f ./docker/docker-compose.yml down
