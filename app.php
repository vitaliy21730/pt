<?php
declare(strict_types=1);

require('./vendor/autoload.php');

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use PT\BINProvider\BINProviderInterface;
use PT\BINProvider\LookupBinlistBINProvider;
use PT\CurrencyRatesProvider\CurrencyRatesProviderInterface;
use PT\CurrencyRatesProvider\ExchangeratesapiCurrencyRatesProvider;
use PT\Processor;
use function DI\create;

$builder = new ContainerBuilder();
$builder->useAttributes(false);

$builder->addDefinitions("config.php");

$builder->addDefinitions([
    BINProviderInterface::class => create(LookupBinlistBINProvider::class),
    CurrencyRatesProviderInterface::class => function (ContainerInterface $c) {
        return new ExchangeratesapiCurrencyRatesProvider(
            $c->get('exchangeratesapi.accessKey'),
            $c->get('exchangeratesapi.https'),
        );
    },
]);

$container = $builder->build();

/** @var Processor $processor */
$processor = $container->get(Processor::class);

$processor->process($argv[1]);
