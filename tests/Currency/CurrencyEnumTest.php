<?php
declare(strict_types=1);

namespace Tests\Currency;

use PHPUnit\Framework\TestCase;
use PT\Currency\CurrencyEnum;

class CurrencyEnumTest extends TestCase
{
    public function testCode() {
        $this->assertSame('UAH', CurrencyEnum::UAH->code());
    }

    public function testFromCode() {
        $this->assertSame(CurrencyEnum::EUR, CurrencyEnum::fromCode('EUR'));
        $this->assertSame(CurrencyEnum::USD, CurrencyEnum::fromCode('usd'));
    }
}
