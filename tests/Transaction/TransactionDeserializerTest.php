<?php
declare(strict_types=1);

namespace Tests\Transaction;

use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use PT\Currency\CurrencyEnum;
use PT\Transaction\TransactionDeserializer;
use PT\Transaction\TransactionDto;
use stdClass;

class TransactionDeserializerTest extends TestCase
{
    private TransactionDeserializer $transactionDeserializer;

    protected function setUp(): void
    {
        $this->transactionDeserializer = new TransactionDeserializer();
    }

    #[DataProvider('validationFailedDataProvider')]
    public function testValidationFailedFromArray(array $data, string $reason): void
    {
        try {
            $this->transactionDeserializer->fromArray($data);
            $this->fail('Expected Exception was not thrown');
        } catch (InvalidArgumentException $ex) {
            $this->assertSame($reason, $ex->getMessage());
        }
    }

    #[DataProvider('successDataProvider')]
    public function testSuccessFromArray(array $data, TransactionDto $expectedTransactionDto): void
    {
        $actualTransactionDto = $this->transactionDeserializer->fromArray($data);

        $this->assertSame($expectedTransactionDto->bin, $actualTransactionDto->bin);
        $this->assertSame($expectedTransactionDto->amount, $actualTransactionDto->amount);
        $this->assertSame($expectedTransactionDto->currency, $actualTransactionDto->currency);
    }

    public static function validationFailedDataProvider() : array {
        return [
            [[], 'Bin should be presented'],

            [['amount' => '3452345', 'currency' => 'usd'], 'Bin should be presented'],
            [['bin' => null, 'amount' => '1', 'currency' => 'usd'], 'Bin should be presented'],

            [['bin' => '9223372036854775808', 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => '-9223372036854775809', 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => 'string', 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => true, 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => new stdClass(), 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => '9223372036854775808.123123', 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],
            [['bin' => [], 'amount' => '1', 'currency' => 'usd'], 'Bin should be valid integer format'],

            [['bin' => '3452345', 'currency' => 'usd'], 'Amount should be presented'],
            [['bin' => '1', 'amount' => null, 'currency' => 'usd'], 'Amount should be presented'],

            [['bin' => '1', 'amount' => true, 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => 'string', 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => '-1.01.1', 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => new stdClass(), 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => [], 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => [1], 'currency' => 'usd'], 'Amount should be valid float value'],
            [['bin' => '1', 'amount' => '1.7976931348624E+308', 'currency' => 'usd'], 'Amount should be valid float value'],


            [['bin' => '3452345', 'amount' => '123.01'], 'Currency should be presented'],
            [['bin' => '3452345', 'amount' => '123.01', 'currency' => null], 'Currency should be presented'],
            [['bin' => '3452345', 'amount' => '123.01', 'currency' => 'EURO'], 'Currency should be valid currency code'],
            [['bin' => '3452345', 'amount' => '123.01', 'currency' => '123'], 'Currency should be valid currency code'],
            [['bin' => '3452345', 'amount' => '123.01', 'currency' => []], 'Currency should be valid currency code'],
            [['bin' => '3452345', 'amount' => '123.01', 'currency' => new stdClass()], 'Currency should be valid currency code'],
        ];
    }

    public static function successDataProvider(): array
    {
        return [
            [
                ['bin' => '123123123', 'amount' => '3452345', 'currency' => 'usd'],
                new TransactionDto(123123123, 3452345, CurrencyEnum::USD),
            ],
            [
                ['bin' => 4236456, 'amount' => '9428.01', 'currency' => 'EUR'],
                new TransactionDto(4236456, 9428.01, CurrencyEnum::EUR),
            ],
            [
                ['bin' => '9223372036854775807', 'amount' => '1.7976931348623E+308', 'currency' => 'EUR'],
                new TransactionDto(9223372036854775807, 1.7976931348623E+308, CurrencyEnum::EUR),
            ],
            [
                ['bin' => '-9223372036854775808', 'amount' => '-2.2250738585072e-308', 'currency' => 'EUR'],
                // Here is a PHP bug, it casts to float if avoid (int)
                new TransactionDto((int)-9223372036854775808,  -2.2250738585072e-308, CurrencyEnum::EUR),
            ],
        ];
    }
}
