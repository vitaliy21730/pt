<?php
declare(strict_types=1);

namespace Tests\Fee;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use PT\BINProvider\BINInfo;
use PT\BINProvider\BINProviderInterface;
use PT\Currency\CurrencyEnum;
use PT\CurrencyRatesProvider\CurrencyRatesProviderInterface;
use PT\Fee\FeeCalculatingException;
use PT\Fee\FeeCalculatingService;
use PT\Transaction\TransactionDto;

class FeeCalculatingServiceTest extends TestCase
{
    /**
     * @throws Exception
     * @throws FeeCalculatingException
     */
    public function testForTransactionEUFee() {
        $transactionDto = new TransactionDto(123, 500, CurrencyEnum::USD);

        $binProvider = $this->createMock(BINProviderInterface::class);
        $binProvider->expects($this->once())
            ->method('getInfo')
            ->with($transactionDto->bin)
            ->willReturn(new BINInfo('fr'));

        $currencyRatesProvider = $this->createMock(CurrencyRatesProviderInterface::class);
        $currencyRatesProvider->expects($this->once())
            ->method('getRate')
            ->with(CurrencyEnum::EUR, CurrencyEnum::USD)
            ->willReturn(1.1);

        $feeCalculatingService = new FeeCalculatingService($binProvider, $currencyRatesProvider);
        $rez = $feeCalculatingService->forTransaction($transactionDto);
        $this->assertSame(4.5455, round($rez, 4, PHP_ROUND_HALF_EVEN));
    }

    /**
     * @throws Exception
     * @throws FeeCalculatingException
     */
    public function testForTransactionNonEUFee() {
        $transactionDto = new TransactionDto(456, 1012, CurrencyEnum::GBP);

        $binProvider = $this->createMock(BINProviderInterface::class);
        $binProvider->expects($this->once())
            ->method('getInfo')
            ->willReturn(new BINInfo('UA'));

        $currencyRatesProvider = $this->createMock(CurrencyRatesProviderInterface::class);
        $currencyRatesProvider->expects($this->once())
            ->method('getRate')
            ->with(CurrencyEnum::EUR, CurrencyEnum::GBP)
            ->willReturn(0.98);

        $feeCalculatingService = new FeeCalculatingService($binProvider, $currencyRatesProvider);
        $rez = $feeCalculatingService->forTransaction($transactionDto);
        $this->assertSame(20.6531, round($rez, 4, PHP_ROUND_HALF_EVEN));
    }

    /**
     * @throws Exception
     */
    public function testForTransactionWhenBINProviderReturnsNull() {
        $transactionDto = new TransactionDto(456, 1012, CurrencyEnum::GBP);

        $binProvider = $this->createMock(BINProviderInterface::class);
        $binProvider->expects($this->once())
            ->method('getInfo')
            ->willReturn(null);

        $currencyRatesProvider = $this->createMock(CurrencyRatesProviderInterface::class);
        $currencyRatesProvider->expects($this->once())
            ->method('getRate')
            ->with(CurrencyEnum::EUR, CurrencyEnum::GBP)
            ->willReturn(0.98);

        $feeCalculatingService = new FeeCalculatingService($binProvider, $currencyRatesProvider);

        $this->expectException(FeeCalculatingException::class);
        $feeCalculatingService->forTransaction($transactionDto);
    }

    /**
     * @throws Exception
     */
    public function testForTransactionWhenRatesProviderReturnsNull() {
        $transactionDto = new TransactionDto(456, 1012, CurrencyEnum::GBP);

        $binProvider = $this->createMock(BINProviderInterface::class);
        $binProvider->method('getInfo')
            ->willReturn(new BINInfo('UA'));

        $currencyRatesProvider = $this->createMock(CurrencyRatesProviderInterface::class);
        $currencyRatesProvider->expects($this->once())
            ->method('getRate')
            ->with(CurrencyEnum::EUR, CurrencyEnum::GBP)
            ->willReturn(null);

        $feeCalculatingService = new FeeCalculatingService($binProvider, $currencyRatesProvider);

        $this->expectException(FeeCalculatingException::class);
        $feeCalculatingService->forTransaction($transactionDto);
    }
}
