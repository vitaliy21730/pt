For running the project you can use makefile commands
Also it's needed to copy config.example.php as config.php 

This code can't be refactored completely without appropriate code infrastructure like: global exceptions handlers, logging, caching system, etc.
Next things are also needed to be implemented, but they weren't, because lack of time and access keys to the services:
* Exceptions logging with full contexts. Now it just outputs by default way.
* Retries for api calls on failure
* Caching api calls results (because info about BINs will change never and info about rates on this resource is changed hourly)
* Use separated class for handling countries, info about them, and checks like isEU()

Also for fourth transaction in input.txt I changed BIN to another, because for 41417360 there isn't info.

Tests run:
php ./vendor/bin/phpunit ./tests

But please consider that I don't have a big experience with unit testing.
