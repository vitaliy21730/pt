<?php
declare(strict_types=1);

return [
    'exchangeratesapi.accessKey' => 'YOUR_ACCESS_KEY',
    'exchangeratesapi.https' => false, // If you have non-free plan then you are available to use https
];
